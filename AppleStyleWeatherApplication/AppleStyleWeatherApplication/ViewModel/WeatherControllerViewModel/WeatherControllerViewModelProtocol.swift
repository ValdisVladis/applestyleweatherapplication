//
//  WeatherControllerViewModelProtocol.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/16/21.
//

import Foundation

enum Units: String {
    case standard = "standard"
    case metric = "metric"
    case imperial = "imperial"
}

protocol WeatherViewModelProtocol {
    var locationNameHandler: ((String?)-> Void)? {get set}
    var completion: ((WeatherControllerViewState)-> Void)? {get set}
    func startFetchingWeatherData()
}

final class WeatherViewModel: WeatherViewModelProtocol {
    var locationNameHandler: ((String?) -> Void)?
    
    var completion: ((WeatherControllerViewState) -> Void)?
    
    
    init() {
        completion?(.initialize)
    }
    
    public func startFetchingWeatherData() {
        LocationManager.shared.getLocation { [weak self] (location) in
            guard let self = self else {return}
            LocationManager.shared.resolveLocationName(with: location) { [weak self](locationName) in
                guard let self = self else {return}
                self.locationNameHandler?(locationName)
            }
            let urlForDataFetching = "\(URLConstants.baseURL)/data/2.5/onecall?lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&units=\(Units.metric)&appid=\(URLConstants.apiKey)"
            DataFetcherService().requestData(urlString: urlForDataFetching) {[weak self] (weather) in
                guard let self = self else {return}
                guard let weather = weather else {return}
                self.completion?(.success(weather))
                print(weather.current)
            }
        }
    }
}
