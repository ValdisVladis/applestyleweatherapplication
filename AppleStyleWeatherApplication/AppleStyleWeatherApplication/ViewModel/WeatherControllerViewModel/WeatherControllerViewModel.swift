//
//  WeatherControllerViewModel.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/16/21.
//

import Foundation

enum WeatherControllerViewState {
    case initialize
    case loading
    case success(WeatherData)
}
