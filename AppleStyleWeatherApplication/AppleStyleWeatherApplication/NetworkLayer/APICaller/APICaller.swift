//
//  APICaller.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/15/21.
//

import Foundation

protocol Networking {
    func dataRequest(urlString: String,  completionHandler: @escaping (Data?, Error?) -> Void)
}

class APICaller: Networking {
    
    func dataRequest(urlString: String,  completionHandler: @escaping (Data?, Error?) -> Void) {
        guard let url = URL(string: urlString) else { return }
        let request = URLRequest(url: url)
        let task = createDataDask(from: request, completion: completionHandler)
        task.resume()
  
        
    }
    
    private func createDataDask(from request: URLRequest, completion: @escaping (Data?, Error?)-> Void) ->URLSessionTask {
        return URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            DispatchQueue.main.async {
                completion(data,error)
            }
        }
        
    }
    
}
