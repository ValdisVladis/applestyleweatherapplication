//
//  DataFetcherServices.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/16/21.
//

import Foundation

class DataFetcherService {
    var networkDataFetcher: DataFetcher!
    
    init(networkDataFetcher: DataFetcher = DataFetcher()) {
        self.networkDataFetcher = networkDataFetcher
    }
    
    func requestData(urlString: String, completion: @escaping (WeatherData?) -> Void) {
        networkDataFetcher.fetchGenericJSONData(urlString: urlString, completion: completion)
        
    }
}
