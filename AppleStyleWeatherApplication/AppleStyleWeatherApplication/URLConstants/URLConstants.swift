//
//  URLConstants.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/15/21.
//

import Foundation

struct URLConstants {
    
    static let baseURL = URL(string:"https://api.openweathermap.org")!
    static let apiKey = "e654a7fc91308a9dc6a2254eb640b0c4"
    
    static var originalBaseURL: URL {
        return baseURL.appendingPathComponent(apiKey)
    }
}
