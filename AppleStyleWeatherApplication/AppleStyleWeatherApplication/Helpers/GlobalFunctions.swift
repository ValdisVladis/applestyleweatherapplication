//
//  GlobalFunctions.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/16/21.
//
import UIKit
import Foundation

public func getWeatherIcon(name: String) -> UIImage {
    return UIImage(named: name) ?? UIImage()
}

public func convertTemperature(temperature: Double, fromInputTempType: UnitTemperature, toOutputTempType: UnitTemperature) -> String {
    let measurementFormatter = MeasurementFormatter()
    measurementFormatter.numberFormatter.maximumFractionDigits = 0
    measurementFormatter.unitOptions = .providedUnit
    let input = Measurement(value: temperature, unit: fromInputTempType)
    let output = input.converted(to: toOutputTempType)
    return measurementFormatter.string(from: output)
  
}
