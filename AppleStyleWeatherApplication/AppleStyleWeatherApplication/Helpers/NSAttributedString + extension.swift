//
//  NSAttributedString + extension.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/19/21.
//

import Foundation

extension NSAttributedString {
    static func createHyperLink(for path: String, in string: String, as substring: String) -> NSAttributedString {
        let nsString = NSString(string: string)
        let substringRange = nsString.range(of: substring)
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttribute(.link, value: path, range: substringRange)
        return attributedString
    }
    
}
