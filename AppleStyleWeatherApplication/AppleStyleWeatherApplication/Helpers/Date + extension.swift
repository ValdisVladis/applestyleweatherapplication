//
//  Date + extension.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/16/21.
//

import Foundation

extension Date {
    func getFormattedDate(format: String, value: Int?) -> String {
        let date = Calendar.current.date(byAdding: .day, value: value ?? Int(), to: Date())
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date ?? Date())
    }
    
    func getFormattedTime(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
