//
//  ViewController.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/15/21.
//

import UIKit
import WebKit

class WeatherViewController: UIViewController {
    
    //MARK: - Variables
    private var weatherViewModelProtocol: WeatherViewModelProtocol!
    private var weatherView: FirstWeatherView!
    //MARK: - Constants
   
    //MARK: - UI
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    //MARK: - ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        weatherViewModelProtocol = WeatherViewModel()
        createWeatherView()
        updateWeatherViewData()
        
     
    }
    @IBAction func popToWeatherChannel(_ sender: UIBarButtonItem) {
        LocationManager.shared.getLocation { (location) in
            let weatherChannelLink = URL(string: "https://weather.com/weather/today/l/\(location.coordinate.latitude),\(location.coordinate.longitude)?par=apple_widget&locale=en_BY")!
            UIApplication.shared.open(weatherChannelLink, options: [:], completionHandler: nil)
        }
    
    }
    
    @IBAction func popToCitySearchViewController(_ sender: UIBarButtonItem) {
        guard let citySearchViewController = storyboard?.instantiateViewController(identifier: "CitySearchViewController") as? CitySearchViewController else {return }
        navigationController?.pushViewController(citySearchViewController, animated: true)
    }
    
    private func createWeatherView() {
        weatherView = FirstWeatherView()
        weatherView.frame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
        scrollView.addSubview(weatherView)
    }
    
    private func updateWeatherViewData() {
        weatherViewModelProtocol.completion = { [weak self] weather in
            guard let self = self else {return}
            self.weatherView.viewState = weather
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        displayOrHidePageControll()
        weatherViewModelProtocol.startFetchingWeatherData()
       
    }
}
private extension WeatherViewController {
    func displayOrHidePageControll() {
        if pageControl.numberOfPages >= 0 {
            self.pageControl.isHidden = true
        }else if pageControl.numberOfPages >= 1 {
            pageControl.isHidden = false
        }
    }
    
    func checkInternetAvailability() {
        if !NetworkMonitor.shared.isConnected {
            UIAlertController.showNetworkUnavailability(on: self, title: "Mobile Data is Turned Off", message: "Turn on mobile data or use Wi-Fi to access data", preferredStyle: .alert)
        }
    }
}
