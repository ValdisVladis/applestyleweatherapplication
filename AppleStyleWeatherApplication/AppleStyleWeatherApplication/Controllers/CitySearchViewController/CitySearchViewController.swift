//
//  CitySearchViewController.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/19/21.
//

import UIKit

class CitySearchViewController: UITableViewController {
    @IBOutlet weak var citySearchTableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    private var dataFetcher = DataFetcherService()
    private var weatherData: WeatherData?
    private var data = [Daily]()
    private let url =
        "https://api.openweathermap.org/data/2.5/onecall?lat=53.9&lon=27.5667&units=metric&appid=e654a7fc91308a9dc6a2254eb640b0c4"
    var completion: ((UILabel)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        configureCitySearchTableView()
        view.backgroundColor = .black
        dataFetcher.requestData(urlString: url) { (result) in
            self.data = result?.daily ?? []
            self.weatherData = result
            self.tableView.reloadData()
            
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    @IBAction func popToWeatherChannelWebsite(_ sender: UIButton) {
        
    }
    
    @IBAction func searchForSpecificCity(_ sender: UIButton) {
        guard let cityListController = self.storyboard?.instantiateViewController(identifier: "CityListTableViewController") as? CityListTableViewController else {return}
        navigationController?.present(cityListController, animated: true, completion: nil)
    }
    
    @IBAction func switchTemperatureFormat(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            let value = weatherData?.current.temp ?? Double()
            let tempToConvert = convertTemperature(temperature: value , fromInputTempType: .celsius, toOutputTempType: .fahrenheit)
            completion = { label in
                label.text = tempToConvert
            }
            self.citySearchTableView.reloadData()
        case 1:
            let value = weatherData?.current.temp ?? Double()
            let tempToConvert = convertTemperature(temperature: value , fromInputTempType: .fahrenheit, toOutputTempType: .celsius)
            completion = { label in
                label.text = tempToConvert
            }
            self.citySearchTableView.reloadData()
        default:
            break
        }
    }
    
    
    // MARK: - Table view data source
    
    //    override func numberOfSections(in tableView: UITableView) -> Int {
    //        // #warning Incomplete implementation, return the number of sections
    //        return 0
    //    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return data.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cityCell = tableView.dequeueReusableCell(withIdentifier: CitySearchTableViewCell.reusableIdentifier, for: indexPath) as? CitySearchTableViewCell else {return UITableViewCell()}
        cityCell.populateCitySearchTableViewCell(with: data)
        
        completion?(cityCell.temperatureLabel)
        return cityCell
        
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
       return true
    }
   
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if tableView.isEditing {
            tableView.isEditing = false
        }else {
            tableView.isEditing = true
        }
       
            self.data.swapAt(sourceIndexPath.row, destinationIndexPath.row)
            
        
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension CitySearchViewController {
    func configureCitySearchTableView() {
        self.citySearchTableView.register(CitySearchTableViewCell.nib(), forCellReuseIdentifier: CitySearchTableViewCell.reusableIdentifier)
        self.citySearchTableView.delegate = self
        self.citySearchTableView.dataSource = self
    }
}
