//
//  FirstWeatherView.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/16/21.
//

import UIKit

class FirstWeatherView: UIView {
    //    MARK: - UI
    @IBOutlet var mainWeatherView: UIView!
    @IBOutlet weak var broadcastTableView: UITableView!
    @IBOutlet weak var timezoneLabel: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var mainTemp: UILabel!
    @IBOutlet weak var hightTemp: UILabel!
    @IBOutlet weak var lowTemp: UILabel!
    // MARK: - Variables
    private var dataFetcher = DataFetcherService()
    var weatherData: WeatherData?
    private var daily = [Daily]()
    private var hourly = [Hourly]()
    private var cellCount = 7
    private var numberOfSections: Int = 1
    var viewState: WeatherControllerViewState = .initialize {
        didSet {
            setNeedsLayout()
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        switch viewState {
        case .initialize:
            print("view has been initialiezed")
        case .loading:
            print("view is loading")
        case .success(let weather):
            self.weatherData = weather
            self.daily = weather.daily ?? []
            self.initialize()
            self.timezoneLabel.text = weather.timezone
            self.weatherDescription.text = weather.current.weather[0].description.capitalized
            self.mainTemp.text = "\(Int(weather.current.temp)) °"
            self.hightTemp.text = "\(Int(weather.daily[0].temp.max)) °"
            self.lowTemp.text = "\(Int(weather.daily[0].temp.min)) °"
            self.broadcastTableView.reloadData()
        }
    }
    private func initialize() {
        Bundle.main.loadNibNamed("FirstWeatherView", owner: self, options: nil)
        addSubview(mainWeatherView)
        mainWeatherView.frame = self.bounds
        mainWeatherView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setupBroadcastTableView()
        
    }
    private func setupBroadcastTableView() {
        broadcastTableView.register(DailyTableViewCell.nib(), forCellReuseIdentifier: DailyTableViewCell.reusableIdentifier)
        broadcastTableView.register(WeatherDescriptionTableViewCell.nib(), forCellReuseIdentifier: WeatherDescriptionTableViewCell.reusableIdentifier)
        broadcastTableView.register(SunriseSunsetTableViewCell.nib(), forCellReuseIdentifier: SunriseSunsetTableViewCell.reusableIdentifier)
        broadcastTableView.register(ChanceOfRainHumidityTableViewCell.nib(), forCellReuseIdentifier: ChanceOfRainHumidityTableViewCell.reusableIdentifier)
        broadcastTableView.register(WindFeelsLikeTableViewCell.nib(), forCellReuseIdentifier: WindFeelsLikeTableViewCell.reusableIdentifier)
        broadcastTableView.register(PrecipitationPressureTableViewCell.nib(), forCellReuseIdentifier: PrecipitationPressureTableViewCell.reusableIdentifier)
        broadcastTableView.register(VisibilityUVIndexTableViewCell.nib(), forCellReuseIdentifier: VisibilityUVIndexTableViewCell.reusableIdentifier)
        broadcastTableView.register(WeatherChannelMapTableViewCell.nib(), forCellReuseIdentifier: WeatherChannelMapTableViewCell.reusableIdentifier)
        broadcastTableView.register(TableHeaderCollectionView.nib(), forCellReuseIdentifier: TableHeaderCollectionView.reusableIdentifier)
        broadcastTableView.delegate = self
        broadcastTableView.dataSource = self
        
    }
   
}
extension FirstWeatherView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let tableViewHeader = tableView.dequeueReusableCell(withIdentifier: TableHeaderCollectionView.reusableIdentifier) as? TableHeaderCollectionView else {return UIView()}
        tableViewHeader.hourlyDataSource = weatherData?.hourly ?? []
        tableViewHeader.currentForecast = weatherData?.current
        return tableViewHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < daily.count {
            return 40
        }else if indexPath.row < daily.count + 1 {
            return 70
        }else if indexPath.row < daily.count + 2 {
            return 70
        }else if indexPath.row < daily.count + 3 {
            return 70
        }else if indexPath.row < daily.count + 4 {
            return 70
        }else if indexPath.row < daily.count + 5 {
            return 70
        }else if indexPath.row < daily.count + 6 {
            return 70
        }else if indexPath.row < daily.count + 7 {
            return 70
        }
        
        return CGFloat()
    }
}
extension FirstWeatherView: UITableViewDataSource {
//        func numberOfSections(in tableView: UITableView) -> Int {
//            return self.numberOfSections
//        }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return daily.count + 7
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < daily.count  {
            guard let dailyCell = tableView.dequeueReusableCell(withIdentifier: DailyTableViewCell.reusableIdentifier, for: indexPath) as? DailyTableViewCell else {return UITableViewCell()}
            let dailyData = daily[indexPath.row]
            dailyCell.dateLabel.text = dailyData.dt.getFormattedDate(format: "EEEE", value: indexPath.row)
            dailyCell.populateTableViewCell(with: dailyData)
            return dailyCell
            
        }else if indexPath.row < daily.count + 1  {
            guard let weatherInfoCell = tableView.dequeueReusableCell(withIdentifier: WeatherDescriptionTableViewCell.reusableIdentifier, for: indexPath) as? WeatherDescriptionTableViewCell else {return UITableViewCell()}
            guard let data = weatherData else {
                return UITableViewCell()
                
            }
            weatherInfoCell.populateWeatherDescriptionCell(with: data)
            return weatherInfoCell
        }else if indexPath.row < daily.count + 2 {
            guard let sunriseSunsetCell = tableView.dequeueReusableCell(withIdentifier: SunriseSunsetTableViewCell.reusableIdentifier, for: indexPath) as? SunriseSunsetTableViewCell else {return UITableViewCell()}
            guard let data = weatherData else {
                return UITableViewCell()
            }
            sunriseSunsetCell.populateSunriseSunsetTableViewCell(with: data)
            return sunriseSunsetCell
        }else if indexPath.row < daily.count + 3 {
            guard let chanceOfRainHumidityCell = tableView.dequeueReusableCell(withIdentifier: ChanceOfRainHumidityTableViewCell.reusableIdentifier, for: indexPath) as? ChanceOfRainHumidityTableViewCell else {return UITableViewCell()}
            guard let data = weatherData else {
                return UITableViewCell()
            }
            chanceOfRainHumidityCell.populateChanceOfRainHumidityTableViewCell(with: data)
            return chanceOfRainHumidityCell
        }else if indexPath.row < daily.count + 4 {
            guard let windFeelsLikeCell = tableView.dequeueReusableCell(withIdentifier: WindFeelsLikeTableViewCell.reusableIdentifier, for: indexPath) as? WindFeelsLikeTableViewCell else {return UITableViewCell()}
            guard let data = weatherData else {return UITableViewCell()}
            windFeelsLikeCell.populateWindFeelsLikeTableViewCell(with: data)
            return windFeelsLikeCell
        }else if indexPath.row < daily.count + 5 {
            guard let precipitationPressureCell = tableView.dequeueReusableCell(withIdentifier: PrecipitationPressureTableViewCell.reusableIdentifier, for: indexPath) as? PrecipitationPressureTableViewCell else {return UITableViewCell()}
            guard let data = weatherData else {return UITableViewCell()}
            precipitationPressureCell.populatePrecipitationPressureTableViewCell(with: data)
            return precipitationPressureCell
        }else if indexPath.row < daily.count + 6 {
            guard let visibilityUVIndexCell = tableView.dequeueReusableCell(withIdentifier: VisibilityUVIndexTableViewCell.reusableIdentifier, for: indexPath) as? VisibilityUVIndexTableViewCell else {return UITableViewCell()}
            guard let data = weatherData else {return UITableViewCell()}
            visibilityUVIndexCell.populateVisibilityUVIndexTableViewCell(with: data)
            return visibilityUVIndexCell
        }else if indexPath.row < daily.count + 7 {
            guard let weatherMapCell = tableView.dequeueReusableCell(withIdentifier: WeatherChannelMapTableViewCell.reusableIdentifier, for: indexPath) as? WeatherChannelMapTableViewCell else {return UITableViewCell()}
            guard let data = weatherData else {return UITableViewCell()}
//            weatherMapCell.populateWeatherChannelMapTableViewCell(with: data)
//            weatherMapCell.updateTextView()
//            weatherMapCell.poptoMapsApp()
            return weatherMapCell
        }
            return UITableViewCell()
       
    }
}
