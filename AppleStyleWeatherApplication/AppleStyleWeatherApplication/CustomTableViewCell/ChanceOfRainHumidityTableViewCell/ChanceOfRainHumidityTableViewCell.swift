//
//  ChanceOfRainHumidityTableViewCell.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/18/21.
//

import UIKit

protocol ChanceOfRainHumidityTableViewCellProtocol {
    func populateChanceOfRainHumidityTableViewCell(with object: WeatherData)
}

class ChanceOfRainHumidityTableViewCell: UITableViewCell {
    
    @IBOutlet weak var chanceOfRainLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    
    static let reusableIdentifier = "ChanceOfRainHumidityTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "ChanceOfRainHumidityTableViewCell" , bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension ChanceOfRainHumidityTableViewCell: ChanceOfRainHumidityTableViewCellProtocol {
    func populateChanceOfRainHumidityTableViewCell(with object: WeatherData) {
        self.chanceOfRainLabel.text = "\(Int(object.current.rain?.oneHh ?? Double())) %"
        self.humidityLabel.text = "\(Int(object.current.humidity)) %"
    }
    
    
}
