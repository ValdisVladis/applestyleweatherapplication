//
//  DailyTableViewCell.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/16/21.
//

import UIKit

protocol DailyTableViewCellProtocol {
    func populateTableViewCell(with object: Daily)
}

class DailyTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var minTemp: UILabel!
    @IBOutlet weak var percentage: UILabel!
    
    static let reusableIdentifier = "DailyTableViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "DailyTableViewCell", bundle: Bundle.main)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
extension DailyTableViewCell: DailyTableViewCellProtocol {
    func populateTableViewCell(with object: Daily) {
        self.weatherIcon.image = getWeatherIcon(name: object.weather[0].icon )
        self.maxTemp.text = "\(Int(object.temp.max ))"
        self.minTemp.text = "\(Int(object.temp.min ))"
        
        if object.weather[0].icon == "11d" {
            self.percentage.text = "\(Int(object.rain ?? Double()))%"
        }else if object.weather[0].icon == "13d" {
            self.percentage.text = "\(Int(object.snow ?? Double()))%"
        }else{
            self.percentage.isHidden = true
        }
    }
}
