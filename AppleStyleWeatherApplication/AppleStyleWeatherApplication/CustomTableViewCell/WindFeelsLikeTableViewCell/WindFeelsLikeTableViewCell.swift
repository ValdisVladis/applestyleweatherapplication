//
//  WindFeelsLikeTableViewCell.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/18/21.
//

import UIKit

protocol WindFeelsLikeTableViewCellProtocol {
    func populateWindFeelsLikeTableViewCell(with object: WeatherData)
}

class WindFeelsLikeTableViewCell: UITableViewCell {
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    
    static let reusableIdentifier = "WindFeelsLikeTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "WindFeelsLikeTableViewCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension WindFeelsLikeTableViewCell: WindFeelsLikeTableViewCellProtocol {
    func populateWindFeelsLikeTableViewCell(with object: WeatherData) {
        self.windLabel.text = "w \(Int(object.current.windSpeed)) km/hr"
        self.feelsLikeLabel.text = "\(Int(object.current.feelsLike)) °"
    }
    
    
}
