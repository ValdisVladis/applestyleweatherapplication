//
//  VisibilityUVIndexTableViewCell.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/18/21.
//

import UIKit

protocol VisibilityUVIndexTableViewCellProtocol {
    func populateVisibilityUVIndexTableViewCell(with object: WeatherData)
}

class VisibilityUVIndexTableViewCell: UITableViewCell {
    @IBOutlet weak var visibilityLabel: UILabel!
    @IBOutlet weak var uvIndexLabel: UILabel!
    
    static let reusableIdentifier = "VisibilityUVIndexTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "VisibilityUVIndexTableViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension VisibilityUVIndexTableViewCell: VisibilityUVIndexTableViewCellProtocol {
    func populateVisibilityUVIndexTableViewCell(with object: WeatherData) {
        self.visibilityLabel.text = "\(object.current.visibility) km"
        self.uvIndexLabel.text = "\(Int(object.current.uvi))"
    }
    
    
}
