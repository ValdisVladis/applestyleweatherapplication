//
//  InsertableView.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/31/21.
//

import UIKit

class InsertableView: UIView {
    @IBOutlet var insertableView: UIView!
    @IBOutlet weak var sunriseSunsetTimeLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var sunriseSunsetIcon: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize() {
        Bundle.main.loadNibNamed("InsertableView", owner: self, options: nil)
//        insertableView.frame = CGRect(x: 0, y: 0, width: 90, height: 100)
        self.addSubview(insertableView)
        self.descriptionLabel.text = "Sunrise"
        self.sunriseSunsetIcon.image = UIImage(named: "sunrise")
        
    }
    
}
