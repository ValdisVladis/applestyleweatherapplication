//
//  WeatherDescriptionTableViewCell.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/17/21.
//

import UIKit

protocol WeatherDescriptionCellProtocol {
    func populateWeatherDescriptionCell(with object: WeatherData)
}

class WeatherDescriptionTableViewCell: UITableViewCell {
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    
    static let reusableIdentifier = "weatherDescriptionCell"
    static func nib() -> UINib {
        return UINib(nibName: "WeatherDescriptionTableViewCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension WeatherDescriptionTableViewCell: WeatherDescriptionCellProtocol {
    func populateWeatherDescriptionCell(with object: WeatherData) {
        self.weatherDescriptionLabel.text = "\(object.current.weather[0].description.capitalized) currently. The high will be \(Int(object.daily[0].temp.max)). The low tonight will be \(Int(object.daily[0].temp.min))."
    }
    
    
}
