//
//  SunriseSunsetTableViewCell.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/18/21.
//

import UIKit

protocol SunriseSunsetTableViewCellProtocol {
    func populateSunriseSunsetTableViewCell(with object: WeatherData)
}

class SunriseSunsetTableViewCell: UITableViewCell {
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    
    static let reusableIdentifier = "SunriseSunsetTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "SunriseSunsetTableViewCell", bundle: nil)
    
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension SunriseSunsetTableViewCell: SunriseSunsetTableViewCellProtocol {
    func populateSunriseSunsetTableViewCell(with object: WeatherData) {
        self.sunriseLabel.text = object.current.sunrise.getFormattedTime(format: "HH:mm")
        self.sunsetLabel.text = object.current.sunset.getFormattedTime(format: "HH:mm")
    }
    
    
}
