//
//  PrecipitationPressureTableViewCell.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/18/21.
//

import UIKit

protocol PrecipitationPressureTableViewCellProtocol {
    func populatePrecipitationPressureTableViewCell(with object: WeatherData)
}

class PrecipitationPressureTableViewCell: UITableViewCell {
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    
    static let reusableIdentifier = "PrecipitationPressureTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "PrecipitationPressureTableViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension PrecipitationPressureTableViewCell: PrecipitationPressureTableViewCellProtocol {
    func populatePrecipitationPressureTableViewCell(with object: WeatherData) {
        self.precipitationLabel.text = "\(Int(object.minutely[0].precipitation)) cm"
        self.pressureLabel.text = "\(object.current.pressure) hPa"
    }
    
    
}
