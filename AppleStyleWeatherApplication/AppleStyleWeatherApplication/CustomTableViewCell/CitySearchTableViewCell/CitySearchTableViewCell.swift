//
//  CitySearchTableViewCell.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/19/21.
//

import UIKit

protocol  CitySearchTableViewCellProtocol {
    func populateCitySearchTableViewCell(with object: [Daily])
}

class CitySearchTableViewCell: UITableViewCell {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    static let reusableIdentifier = "CitySearchTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "CitySearchTableViewCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension CitySearchTableViewCell: CitySearchTableViewCellProtocol {
    func populateCitySearchTableViewCell(with object: [Daily]) {
        self.temperatureLabel.text = "\(object[0].temp.max) °"
    }
    
    
}
