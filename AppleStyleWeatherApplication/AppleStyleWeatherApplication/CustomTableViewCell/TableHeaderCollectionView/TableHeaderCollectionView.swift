//
//  TableHeaderCollectionView.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 8/1/21.
//

import UIKit

class TableHeaderCollectionView: UITableViewCell {
    @IBOutlet weak var hourlyCollectionViewCell: UICollectionView!
    private var numberOfCells = 8
    var hourlyDataSource: [Hourly] = []
    var currentForecast: Current?
    static let reusableIdentifier = "TableHeaderCollectionView"
    static func nib() -> UINib {
        return UINib(nibName: "TableHeaderCollectionView", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        hourlyCollectionViewCell.delegate = self
        hourlyCollectionViewCell.dataSource = self
        hourlyCollectionViewCell.register(HourlyCollectionViewCell.nib(), forCellWithReuseIdentifier: HourlyCollectionViewCell.reusableIdentifier)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
extension TableHeaderCollectionView: UICollectionViewDelegate {
    
}
extension TableHeaderCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hourlyDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let hourlyCell = collectionView.dequeueReusableCell(withReuseIdentifier: HourlyCollectionViewCell.reusableIdentifier, for: indexPath) as? HourlyCollectionViewCell else {return UICollectionViewCell()}
        let object = hourlyDataSource[indexPath.item]
        if indexPath.item == 0 {
            hourlyCell.dateLabel.text = "Now"
            hourlyCell.percentage.isHidden = true
        }else {
            hourlyCell.populateCollectionViewCell(with:object )
        }
        var date = object.dt.getFormattedTime(format: "HH")
        let sunrise = currentForecast?.sunrise.getFormattedTime(format: "HH")
        var sunset = currentForecast?.sunset.getFormattedTime(format: "HH")
        
        if date == sunrise {
            hourlyCell.dateLabel.text = currentForecast?.sunrise.getFormattedTime(format: "HH:mm")
            hourlyCell.tempLabel.text = "Sunrise"
            hourlyCell.weatherIcon.image = UIImage(named: "sunrise")
        }else if date == sunset {
            hourlyCell.dateLabel.text = currentForecast?.sunset.getFormattedTime(format: "HH:mm")
            hourlyCell.tempLabel.text = "Sunset"
            hourlyCell.weatherIcon.image = UIImage(named: "sunset")
        }
        return hourlyCell
    }
    
}
extension TableHeaderCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewFrame = collectionView.frame
        let cellWidth = collectionViewFrame.width / CGFloat(numberOfCells)
        return CGSize(width: cellWidth, height: collectionView.frame.height)
    }
}
