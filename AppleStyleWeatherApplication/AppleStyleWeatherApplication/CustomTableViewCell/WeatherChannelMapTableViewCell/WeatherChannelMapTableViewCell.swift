//
//  WeatherChannelMapTableViewCell.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/18/21.
//
import MapKit
import UIKit

protocol WeatherChannelMapTableViewCellProtocol {
    func populateWeatherChannelMapTableViewCell(with object: WeatherData)
}

class WeatherChannelMapTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mapLinkLabel: UITextView!
    
    static let reusableIdentifier = "WeatherChannelMapTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "WeatherChannelMapTableViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
//    func updateTextView() {
//        let path = "https://weather.com/weather/today/l/53.90,27.55?par=apple_widget&locale=en_BY"
//        let text = mapLinkLabel.text ?? ""
//        let attributedString = NSAttributedString.createHyperLink(for: path, in: text, as: "Open in Maps")
//        mapLinkLabel.attributedText = attributedString
//    }
    
//    func poptoMapsApp() {
//        LocationManager.shared.getLocation { (location) in
//            let latitude = location.coordinate.latitude
//            let longitude = location.coordinate.longitude
//            let coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
//            let regionDistance: CLLocationDistance = 1000
//            let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
//            let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
//                           MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
//            ]
//            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
//            let mapItem = MKMapItem(placemark: placemark)
//            LocationManager.shared.resolveLocationName(with: location) { (mapItemName) in
//                mapItem.name = mapItemName
//                mapItem.openInMaps(launchOptions: options)
//            }
//            
//        }
//    }
    
}
extension WeatherChannelMapTableViewCell: WeatherChannelMapTableViewCellProtocol {
    func populateWeatherChannelMapTableViewCell(with object: WeatherData) {
//        mapLinkLabel.text = "Weather for \(object.timezone). Open in Maps"
        
    }
}
