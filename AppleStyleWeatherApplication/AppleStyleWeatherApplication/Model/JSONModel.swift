//
//  JSONModel.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/16/21.
//

import Foundation

struct WeatherData: Decodable {
    let lat: Double
    let lon: Double
    var timezone: String
    let current: Current
    let minutely: [Minutley]
    var hourly: [Hourly]
    var daily: [Daily]
}

struct Current: Decodable {
    let dt: Date
    let temp: Double
    let feelsLike: Double
    let humidity: Double
    let sunrise: Date
    let sunset: Date
    let pressure: Int
    let uvi: Double
    let clouds: Int
    let visibility: Int
    let windSpeed: Double
    let windDeg: Int
    let weather: [CurrentWeather]
    let rain: RainHourlyCurrent?
    let snow: SnowForecast?
}

struct Hourly: Decodable {
    let dt: Date
    let temp: Double
    let pop: Double?
    let rain: RainHourlyCurrent?
    let snow: SnowForecast?
    let weather: [HourlyWeather]
}

struct Minutley: Decodable {
    let dt: Date
    let precipitation: Double
}

struct RainHourlyCurrent: Decodable {
    var oneHh: Double?
    private enum CodingKeys: String, CodingKey {
        case oneHh = "1h"
    }
}

struct SnowForecast: Decodable {
    var oneHh: Double?
    private enum CodingKeys: String, CodingKey {
        case oneHh = "1h"
    }
    
}

struct Daily: Decodable {
    let dt: Date
    let temp: DailyTemp
    let rain: Double?
    let snow: Double?
    let weather: [DailyWeather]
    
}

struct CurrentWeather: Decodable {
    let description: String
    let main: String
    let icon: String
}

struct HourlyWeather: Decodable {
    let icon: String
}

struct DailyTemp: Decodable {
    let min: Double
    let max: Double
    let day: Double
    let night: Double
}

struct DailyWeather: Decodable {
    let icon: String
}
