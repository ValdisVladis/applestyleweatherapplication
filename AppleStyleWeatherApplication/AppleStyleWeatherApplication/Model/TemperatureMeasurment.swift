//
//  TemperatureMeasurment.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/20/21.
//

import Foundation

public enum TemperatureMeasurment {
    case celsius
    case fahrenheit
}
