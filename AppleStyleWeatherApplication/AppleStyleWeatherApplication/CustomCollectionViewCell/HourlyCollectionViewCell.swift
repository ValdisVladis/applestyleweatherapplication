//
//  HourlyCollectionViewCell.swift
//  AppleStyleWeatherApplication
//
//  Created by Владислав on 7/16/21.
//

import UIKit

protocol HourlyCollectionViewCellProtocol {
    func populateCollectionViewCell(with object: Hourly)
}

class HourlyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var percentage: UILabel!
    
    static let reusableIdentifier = "HourlyCollectionViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "HourlyCollectionViewCell", bundle: Bundle.main)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
}
extension HourlyCollectionViewCell: HourlyCollectionViewCellProtocol {
    func populateCollectionViewCell(with object: Hourly) {
        self.dateLabel.text = object.dt.getFormattedTime(format: "HH")
        self.tempLabel.text = "\(Int(object.temp )) °"
        self.weatherIcon.image = getWeatherIcon(name: object.weather[0].icon )
        
        if object.weather[0].icon == "11d" {
            self.percentage.text = "\(Int(object.rain?.oneHh ?? Double()))"
        }else if object.weather[0].icon == "13d" {
            self.percentage.text = "\(Int(object.snow?.oneHh ?? Double()))"
        }else {
            self.percentage.isHidden = true
        }
    }
}
